# Generated by Django 2.1.7 on 2019-03-12 08:23

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0004_auto_20190312_1122'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end',
            field=models.DateTimeField(default=datetime.datetime(2019, 3, 12, 11, 23, 44, 124924)),
        ),
    ]
