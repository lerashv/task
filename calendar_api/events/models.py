from django.db import models
from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from datetime import datetime

# Create your models here.

class APIUserManager(BaseUserManager):

    def create_user(self, email, password=None):
        if not email:
            raise ValueError("You should privide an email!")
        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):     
        user = self.create_user(email, password=password)
        user.is_admin = True
        user.save(using=self._db)
        return user

class APIUser(AbstractBaseUser):
    
    email = models.EmailField(unique=True)
    country = models.CharField(max_length=64, blank=True)
    is_admin = models.BooleanField(default=False)

    objects = APIUserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin




class Event(models.Model):

    REMINDER_CHOICES = [
        (1, "1 hour before"),
        (2,"2 hours before"),
        (4,"4 hours before"),
        (24, "A day before"),
        (168,"A week before"),
    ]

    name = models.CharField(max_length=200)
    start = models.DateTimeField()
    end = models.DateTimeField(default=datetime.today())
    reminder = models.CharField(max_length=2, choices=REMINDER_CHOICES, blank=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='events')


    def __str__(self):
        return f"{self.name}, {self.owner}"   

    class Meta:
        ordering = ['start']