from rest_framework import serializers

from .models import Event, APIUser

class EventSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source = 'owner.email')
   
    class Meta:
        model = Event
        fields = ('id', 'name', 'start','end', 'reminder', 'owner')

class UserSerializer(serializers.ModelSerializer):
    events = serializers.PrimaryKeyRelatedField(many = True, queryset = Event.objects.all())

    class Meta:
        model = APIUser
        fields = ('id', 'email', 'events')
