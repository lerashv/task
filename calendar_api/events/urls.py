from django.urls import path
from rest_framework import routers

from . import views
from .views import EventViewSet, UserViewSet

router=routers.SimpleRouter()
router.register(r'events', EventViewSet)
router.register(r'users', UserViewSet)
urlpatterns=router.urls

