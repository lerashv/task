from django.shortcuts import render
from django.http import Http404
from rest_framework import generics, viewsets
from rest_framework import status, permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from datetime import datetime, date
from ics import Calendar
from urllib.request import urlopen 
import re

from .models import Event, APIUser
from .serializers import EventSerializer, UserSerializer


# Create your views here.

class EventViewSet(viewsets.ModelViewSet):

    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, )

    def get_queryset(self):
        """
        Returns a list of current user's events only.

        """
        try:
            queryset = self.request.user.events.filter(owner=self.request.user)
        except AttributeError:
            raise Http404
        return queryset
        

    def perform_create(self, serializer):
        # If the end of the event isn't provided, switches to 23:59 of the same date as 'start'
        serializer.save(owner=self.request.user)
        if not self.request.data['end']:
            date = datetime.fromisoformat(self.request.data['start']).replace(hour=23, minute=59)
            serializer.save(end=datetime.isoformat(date))


    @action(detail=False, url_path='today')
    def get_today_events(self, request, pk=None):        
        user = self.request.user
        events = user.events.filter(start__contains=date.isoformat(datetime.today().date()))
        if not events.exists():
            return Response('No events for today.')
        serializer = EventSerializer(events, many=True) 

        return Response(serializer.data)



    @action(detail=False, url_path='daily')
    def get_daily_events(self, request, pk=None):
        d = re.compile('[0-9]{4}-[0-9]{2}-[0-9]{2}')    
        required_date = self.request.query_params.get('date', None)

        if not d.match(required_date):
            return Response('Error. Incorrect input.')

        user = self.request.user
        events = user.events.filter(start__contains=required_date)

        if not events.exists():
            return Response('No events for this date.')
            
        serializer = EventSerializer(events, many=True)

        return Response(serializer.data)



    @action(detail=False, url_path='holidays')
    def get_monthly_holidays(self, request, pk=None):        
        country = self.request.user.country
        if not country:
            return Response('To get holiday list you must provide \'country\' attribute.')

        url = f'http://www.officeholidays.com/ics/ics_country.php?tbl_country={country}'
        try:
            c = Calendar(urlopen(url).read().decode('iso-8859-1'))
        except:
            raise Http404 

        m = re.compile('[0-9]{1,2}')
        required_month = self.request.query_params.get('month', None)
        if not m.match(required_month):
            return Response('Error. Incorrect input.')

        events = [event for event in c.events if event.begin.month==int(required_month) and event.begin.year==date.today().year]
        if not events:
            return Response('No holidays for this month.')
            
        return Response(f"{event.name}, {event.begin.datetime.date()}" for event in events)



class UserViewSet(viewsets.ModelViewSet):
    queryset = APIUser.objects.all()
    serializer_class = UserSerializer


