Calendar API, Junior Edition

To see how it works follow these steps:
- clone the repo
- swith to dir 'calendar_api' (and if you need install required packages using 'pip install -r requirements.txt') 
- launch the server using 'python manage.py runserver' command
- in a browser go to .../events url , which is a default one

How to use this API:

First, you must login into your account to manage your events.
(Since I had some troubles with custom registration interface, you can only add user via command line or just login 
using prepared account data(recommended)). 

email - foo@foo.com
pass - f00f00f00 (0 - zero!)

To get the required data use these urls:

1. /events - all the user's events
2. /events/<id> - get access to a particular event
3. /events/today - special case - get list of 'today' events
4. /events/daily/?date=<your_date> - get list of events for a particular date, pass date in format 'yyyy-mm-dd'
5. /events/holidays/?month=<your_month_number> - get list of your national holidays for a particular month

NOTE: To make API return JSON-formatted data use suffix '?format=json' with each  url,
e.g.:

/events/?format=json

or /events/daily/?format=json&date=<your_date> if you pass query parameters to url.


P.S. hope it works
